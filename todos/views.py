from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList
from todos.forms import TodoForm

# Create your views here.


def todo_list(request):
    lists = TodoList.objects.all()
    context = {
        "lists": lists,
    }

    return render(request, "todos/list.html", context)


def show_list(request, id):
    todo_item_list = get_object_or_404(TodoList, id=id)

    context = {
        "todo_item_list": todo_item_list,
    }
    return render(request, "todos/detail.html", context)


def todo_list_create(request):
    if request.method == "POST":
        form = TodoForm(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_list")
    else:
        form = TodoForm

    context = {
        "form": form,
    }
    return render(request, "todos/create.html", context)
